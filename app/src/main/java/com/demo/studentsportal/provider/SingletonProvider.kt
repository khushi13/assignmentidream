package com.demo.studentsportal.provider

import com.demo.studentsportal.ui.datamodels.ReportItemModel
import com.demo.studentsportal.ui.utils.UIConstants
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.core.Repo
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

object SingletonProvider {

    private var firebaseAuth: FirebaseAuth
    private var firebaseDatabase: FirebaseDatabase
    private lateinit var studentClass: String
    private var data : StudentData
    private var history: HistoryManager

    init {
        firebaseAuth = Firebase.auth
        firebaseDatabase = Firebase.database
        data = StudentData()
        history = HistoryManager()
    }

    fun getFirebaseAuthInstance(): FirebaseAuth {
        return firebaseAuth
    }

    fun getDatabaseInstance(): FirebaseDatabase {
        return firebaseDatabase
    }

    fun getStudentClass(): String = data.getClassName()

    fun getHistory(): ArrayList<ReportItemModel> = history.getHistory()

    class StudentData {

        companion object {
            private lateinit var sClassName: String

            fun setClassName(sClass: String) {
                sClassName = sClass
            }
        }
        fun getClassName(): String = sClassName
    }

    class HistoryManager {

        companion object {
            private var history: ArrayList<ReportItemModel> = arrayListOf()

            fun addItemToHistory(reportItemModel: ReportItemModel) {
                var shouldAdd = true
                if (history.isNotEmpty()) {
                    for (h in history) {
                        if (h == reportItemModel) {
                            shouldAdd = false;
                        }
                    }
                }
                if (shouldAdd) {
                    history.add(reportItemModel)
                    getDatabaseInstance().reference
                        .child(getFirebaseAuthInstance().currentUser!!.uid)
                        .child((UIConstants.FirebaseConstants.student_history))
                        .setValue(history)
                }
            }
        }

        fun getHistory(): ArrayList<ReportItemModel> = history
    }
}