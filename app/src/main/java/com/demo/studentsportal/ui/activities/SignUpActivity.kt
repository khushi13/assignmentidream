package com.demo.studentsportal.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.databinding.DataBindingUtil
import com.demo.studentsportal.R
import com.demo.studentsportal.databinding.ActivitySignUpBinding
import com.demo.studentsportal.ui.db.SharedPrefHelper
import com.demo.studentsportal.provider.SingletonProvider
import com.demo.studentsportal.ui.utils.UIConstants

class SignUpActivity : BaseActivity() {

    private lateinit var mBinding: ActivitySignUpBinding

    override fun getActivityName(): String = SignUpActivity::class.java.simpleName

    override fun bindLayout() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding.btnSignUp.setOnClickListener {
            if (mBinding.etSignUpName.text.isNullOrEmpty() ||
                mBinding.etSignUpEmail.text.isNullOrEmpty() ||
                !Patterns.EMAIL_ADDRESS.matcher(mBinding.etSignUpEmail.text).matches() ||
                mBinding.etSignUpPassword.text.isNullOrEmpty() ||
                mBinding.etSignUpPassword.text.length < 4 ||
                mBinding.etSignUpBoard.text.isNullOrEmpty() ||
                mBinding.etSignUpClass.text.isNullOrEmpty()
            ) {
                showToast("Enter all fields...")
                return@setOnClickListener
            }

            showProgressDialog()
            SingletonProvider.getFirebaseAuthInstance().createUserWithEmailAndPassword(
                mBinding.etSignUpEmail.text.toString(), mBinding.etSignUpPassword.text.toString()
            ).addOnCompleteListener(this) { task ->
                hideProgressDialog()
                if (task.isSuccessful) {
                    debugLog("profile created successfully")
                    SingletonProvider.getDatabaseInstance().reference
                        .child(SingletonProvider.getFirebaseAuthInstance().currentUser!!.uid)
                        .child((UIConstants.FirebaseConstants.student_class))
                        .setValue(mBinding.etSignUpClass.text.toString())

                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    debugLog("create user failure")
                    showToast("Authentication Failed!")
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        /*val currentUser = auth.currentUser
        if (currentUser != null) {

        }*/
    }
}