package com.demo.studentsportal.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.studentsportal.R
import com.demo.studentsportal.databinding.ActivityHomeBinding
import com.demo.studentsportal.provider.SingletonProvider
import com.demo.studentsportal.ui.DataHelper
import com.demo.studentsportal.ui.adapters.SubjectsListAdapter
import com.demo.studentsportal.ui.datamodels.RootModel
import com.demo.studentsportal.ui.datamodels.SubjectModel
import com.demo.studentsportal.ui.db.SharedPrefHelper
import com.demo.studentsportal.ui.interfaces.ISubjectClickListener
import com.demo.studentsportal.ui.utils.UIConstants
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_home.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader


class HomeActivity : BaseActivity(), ISubjectClickListener {

    private lateinit var mBinding: ActivityHomeBinding
    private var subjectList: ArrayList<SubjectModel> = arrayListOf()
    private var hashMap: HashMap<String, JSONArray> = HashMap()
    private var studentClass: String = ""

    override fun getActivityName(): String = HomeActivity::class.java.simpleName

    override fun bindLayout() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showProgressDialog()
        SingletonProvider.getDatabaseInstance().reference
            .child(SingletonProvider.getFirebaseAuthInstance().currentUser!!.uid)
            .child(UIConstants.FirebaseConstants.student_class)
            .get().addOnSuccessListener {
                hideProgressDialog()
                studentClass = it.value as String
                debugLog("fetch success:: $studentClass")

                //set value to singleton class
                SingletonProvider.StudentData.setClassName(studentClass)

                // add items in list
                addItemsFromJSON()

                // attach recycler view
                if (subjectList.isNotEmpty()) {
                    val adapter = SubjectsListAdapter(subjectList, this)
                    rv_subject_list.layoutManager = LinearLayoutManager(this)
                    rv_subject_list.adapter = adapter
                } else {
                    debugLog("list is empty")
                    showToast("No Subject found!!!")
                }

            }.addOnFailureListener {
                hideProgressDialog()
                debugLog("firebase database: onCancelled with error: ${it.message}")
                showToast("Something went wrong!!! ${it.message}")
            }

    }

    private fun addItemsFromJSON() {

        hashMap = DataHelper.getClassWiseSubjectHashmap(this)
        debugLog("data from json: hashmap: $hashMap")

        if (hashMap.isEmpty()) return

        val subjects: JSONArray = hashMap[studentClass]!!

        for (i in 0 until subjects.length()) {
            val model = SubjectModel(subjects.getJSONObject(i).getString("id"),
                subjects.getJSONObject(i).getString("name"))
            subjectList.add(model)
        }
        debugLog("final subject list:: $subjectList")

    }

    override fun onSubjectClick(id: String) {
        debugLog("onSubjectClick() with id: $id")
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(UIConstants.SUBJECT_ID, id)
        startActivity(intent)
    }

}