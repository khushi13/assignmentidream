package com.demo.studentsportal.ui.utils

object UIConstants {

    const val SUBJECT_ID = "subject_id"

    const val VIDEOS = "Videos"
    const val BOOKS = "Books"


    object FirebaseConstants {
        /** Firebase key*/
        const val student_class = "student_class"
        const val student_history = "student_history"

    }
}