package com.demo.studentsportal.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.studentsportal.R
import com.demo.studentsportal.databinding.FragmentBooksBinding
import com.demo.studentsportal.provider.SingletonProvider
import com.demo.studentsportal.ui.DataHelper
import com.demo.studentsportal.ui.adapters.BooksListAdapter
import com.demo.studentsportal.ui.datamodels.BookModel
import com.demo.studentsportal.ui.datamodels.ReportItemModel
import com.demo.studentsportal.ui.db.SharedPrefHelper
import com.demo.studentsportal.ui.interfaces.IBookClickListener
import com.demo.studentsportal.ui.utils.ItemTypeEnum
import com.demo.studentsportal.ui.utils.UIConstants
import java.util.*
import kotlin.collections.HashMap

class BooksFragment : BaseFragment(), IBookClickListener {

    private lateinit var mBinding: FragmentBooksBinding
    private lateinit var selectedSubjectId: String

    override fun getFragmentName(): String = BooksFragment::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_books, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.takeIf { it.containsKey(UIConstants.SUBJECT_ID) }?.apply {
            selectedSubjectId = this.getString(UIConstants.SUBJECT_ID)!!
        }
        val list = DataHelper.getSubjectWiseBooksList(
            requireActivity(),
            SingletonProvider.getStudentClass(), selectedSubjectId
        )
        if (list != null && list.isNotEmpty()) {
            mBinding.tvNoBooks.visibility = View.GONE
            val adapter = BooksListAdapter(list, this)
            mBinding.rvBooksList.layoutManager = LinearLayoutManager(requireActivity())
            mBinding.rvBooksList.adapter = adapter

        } else {
            mBinding.tvNoBooks.visibility = View.VISIBLE
        }
    }

    override fun onBookClick(bookModel: BookModel) {
        debugLog("onBookClick() with model= $bookModel")
        val model = ReportItemModel(
            name = bookModel.bookName,
            id = bookModel.id,
            onlineLink = bookModel.onlineLink,
            topicName = bookModel.topicName,
            itemType = ItemTypeEnum.TYPE_BOOK.getItemType(),
            isOpened = true
        )
        SingletonProvider.HistoryManager.addItemToHistory(model)

        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(bookModel.onlineLink)))
    }
}