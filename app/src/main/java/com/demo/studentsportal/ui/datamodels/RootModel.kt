package com.demo.studentsportal.ui.datamodels

import org.json.JSONObject

data class RootModel (var subjects: JSONObject, var topics: JSONObject)