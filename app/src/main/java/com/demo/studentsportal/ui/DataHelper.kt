package com.demo.studentsportal.ui

import android.content.Context
import android.util.Log
import com.demo.studentsportal.R
import com.demo.studentsportal.ui.datamodels.BookModel
import com.demo.studentsportal.ui.datamodels.RootModel
import com.demo.studentsportal.ui.datamodels.VideoModel
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

class DataHelper {

    companion object {
        private fun debugLog(msg: String) {
            Log.d("DataHelper", msg)
        }

        fun getClassWiseSubjectHashmap(context: Context): HashMap<String, JSONArray> {
            val hashMap: HashMap<String, JSONArray> = HashMap()
            try {

                val jsonDataString = DataHelper().readJSONDataFromFile(context)
                val jsonObject = JSONObject(jsonDataString)
                val rootModel = RootModel(
                    jsonObject.getJSONObject("subjects"),
                    jsonObject.getJSONObject("topics")
                )
                val cbseEnglishData = rootModel.subjects
                    .getJSONObject("cbse")
                    .getJSONObject("english")

                val x: Iterator<String> = cbseEnglishData.keys()
                while (x.hasNext()) {
                    val key = x.next()
                    hashMap[key] = cbseEnglishData.getJSONArray(key)
                }

                debugLog("data from json: hashmap: $hashMap")

            } catch (e: JSONException) {
                debugLog("addItemsFromJSON: $e")
            } catch (e: IOException) {
                debugLog("addItemsFromJSON: $e")
            }

            return hashMap
        }

        fun getSubjectWiseBooksList(
            context: Context,
            className: String,
            selectedSubjectId: String
        ): ArrayList<BookModel>? {
            val list : ArrayList<BookModel> = arrayListOf()
            try {

                val jsonDataString = DataHelper().readJSONDataFromFile(context)
                val jsonObject = JSONObject(jsonDataString)
                val rootModel = RootModel(
                    jsonObject.getJSONObject("subjects"),
                    jsonObject.getJSONObject("topics")
                )
                val subjectsData = rootModel.topics
                    .getJSONObject("cbse")
                    .getJSONObject("english")
                    .getJSONObject(className)
                    .getJSONObject("subjects")

                val tempHashMap: HashMap<String, JSONArray> = HashMap()
                val x: Iterator<String> = subjectsData.keys()
                while (x.hasNext()) {
                    val key = x.next()
                    if (subjectsData.getJSONObject(key).has("books_ncert")) {
                        tempHashMap[key] =
                            subjectsData.getJSONObject(key).getJSONArray("books_ncert")
                    }
                }

                if (tempHashMap.isEmpty() || !tempHashMap.containsKey(selectedSubjectId)) {
                    // if data not available
                    debugLog("temp map empty")
                    return null
                }
                val relevantBooks: JSONArray = tempHashMap[selectedSubjectId]!!

                debugLog("relevantBooks:: $relevantBooks")
                val obj = JSONArray()
                for (i in 0 until relevantBooks.length()) {
                    debugLog("relevant item:: ${relevantBooks.getJSONObject(i)}")
                    debugLog(
                        "relevant item topics::: ${
                            relevantBooks.getJSONObject(i).getJSONArray("topics")
                        }"
                    )
                    val tempArray = relevantBooks.getJSONObject(i).getJSONArray("topics")
                    for (k in 0 until tempArray.length()) {
                        obj.put(tempArray.getJSONObject(k))
                    }

                }
                if (!obj.isNull(0)) {
                    for (i in 0 until obj.length()) {
                        val item = obj.getJSONObject(i)
                        val key = item.getString("id")
                        val bookModel = BookModel(
                            item.getString("name"),
                            key, item.getString("onlineLink"),
                            item.getString("topicName")
                        )
                        list.add(bookModel)
                    }
                }
                debugLog("data from json: list: $list")

            } catch (e: JSONException) {
                debugLog("addItemsFromJSON: ${e.message}")
            } catch (e: IOException) {
                debugLog("addItemsFromJSON: ${e.message}")
            }

            return list
        }

        fun getSubjectWiseVideosList(
            context: Context,
            className: String,
            selectedSubjectId: String
        ): ArrayList<VideoModel>? {
            val list : ArrayList<VideoModel> = arrayListOf()
            try {

                val jsonDataString = DataHelper().readJSONDataFromFile(context)
                val jsonObject = JSONObject(jsonDataString)
                val rootModel = RootModel(
                    jsonObject.getJSONObject("subjects"),
                    jsonObject.getJSONObject("topics")
                )
                val subjectsData = rootModel.topics
                    .getJSONObject("cbse")
                    .getJSONObject("english")
                    .getJSONObject(className)
                    .getJSONObject("subjects")

                val tempHashMap: HashMap<String, JSONArray> = HashMap()
                val x: Iterator<String> = subjectsData.keys()
                while (x.hasNext()) {
                    val key = x.next()
                    if (subjectsData.getJSONObject(key).has("video_lessons")) {
                        tempHashMap[key] =
                            subjectsData.getJSONObject(key).getJSONArray("video_lessons")
                    }
                }

                if (tempHashMap.isEmpty() || !tempHashMap.containsKey(selectedSubjectId)) {
                    // if data not available
                    debugLog("temp map empty")
                    return null
                }
                val relevantVideos: JSONArray = tempHashMap[selectedSubjectId]!!

                debugLog("relevantBooks:: $relevantVideos")
                val obj = JSONArray()
                for (i in 0 until relevantVideos.length()) {
                    debugLog("relevant item:: ${relevantVideos.getJSONObject(i)}")
                    debugLog(
                        "relevant item topics::: ${
                            relevantVideos.getJSONObject(i).getJSONArray("topics")
                        }"
                    )
                    val tempArray = relevantVideos.getJSONObject(i).getJSONArray("topics")
                    for (k in 0 until tempArray.length()) {
                        obj.put(tempArray.getJSONObject(k))
                    }

                }
                if (!obj.isNull(0)) {
                    for (i in 0 until obj.length()) {
                        val item = obj.getJSONObject(i)
                        val key = item.getString("id")
                        val videoModel = VideoModel(
                            item.getString("name"),
                            key, item.getString("onlineLink"),
                            item.getString("topicName")
                        )
                        list.add(videoModel)
                    }
                }
                debugLog("data from json: list: $list")

            } catch (e: JSONException) {
                debugLog("addItemsFromJSON: ${e.message}")
            } catch (e: IOException) {
                debugLog("addItemsFromJSON: ${e.message}")
            }

            return list
        }

    }

    @Throws(IOException::class)
    private fun readJSONDataFromFile(context: Context): String? {
        var inputStream: InputStream? = null
        val builder = StringBuilder()
        try {
            var jsonString: String? = null
            inputStream = context.resources.openRawResource(R.raw.demo_task)
            val bufferedReader = BufferedReader(
                InputStreamReader(inputStream, "UTF-8")
            )
            while (bufferedReader.readLine().also { jsonString = it } != null) {
                builder.append(jsonString)
            }
        } finally {
            inputStream?.close()
        }
        return String(builder)
    }

}