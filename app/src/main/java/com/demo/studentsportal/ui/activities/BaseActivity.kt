package com.demo.studentsportal.ui.activities

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.demo.studentsportal.ui.interfaces.IBaseView

abstract class BaseActivity : FragmentActivity(), IBaseView {

    abstract fun getActivityName() : String
    abstract fun bindLayout()

    private var activityName: String = BaseActivity::class.java.simpleName
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindLayout()
        activityName = getActivityName()
    }

    override fun showToast(msg: String) {
        Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT).show()
    }

    override fun debugLog(msg: String) {
        Log.d(activityName, msg)
    }

    override fun showProgressDialog() {
        debugLog("showProgressDialog")
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage("Loading...")
        progressDialog!!.show()
    }

    override fun hideProgressDialog() {
        if (progressDialog != null) progressDialog!!.dismiss()
    }
    
}