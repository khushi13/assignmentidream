package com.demo.studentsportal.ui.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.demo.studentsportal.ui.fragments.BaseFragment
import com.demo.studentsportal.ui.fragments.BooksFragment
import com.demo.studentsportal.ui.fragments.VideosFragment
import com.demo.studentsportal.ui.utils.UIConstants

class ViewPagerAdapter(
    private val subjectId: String,
    fragmentActivity: FragmentActivity
) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        val fragment: BaseFragment = if (position == 0) {
            VideosFragment()
        } else {
            BooksFragment()
        }
        fragment.arguments = Bundle().apply {
            putString(UIConstants.SUBJECT_ID, subjectId)
        }
        return fragment
    }

}