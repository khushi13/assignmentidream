package com.demo.studentsportal.ui.interfaces

import com.demo.studentsportal.ui.datamodels.BookModel

interface IBookClickListener {

    fun onBookClick(bookModel: BookModel)
}