package com.demo.studentsportal.ui.interfaces

interface IBaseView {

    fun showToast(msg: String)

    fun debugLog(msg: String)

    fun showProgressDialog()

    fun hideProgressDialog()
}