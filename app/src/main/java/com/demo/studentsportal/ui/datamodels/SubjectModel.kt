package com.demo.studentsportal.ui.datamodels

data class SubjectModel(val id: String, val name: String)
