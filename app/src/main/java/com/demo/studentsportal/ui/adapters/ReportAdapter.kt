package com.demo.studentsportal.ui.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.demo.studentsportal.R
import com.demo.studentsportal.ui.datamodels.ReportItemModel
import com.demo.studentsportal.ui.utils.ItemTypeEnum

class ReportAdapter(
    private val list: List<ReportItemModel>
) :
    RecyclerView.Adapter<ReportAdapter.BaseViewHolder>() {

    private val item_type_video = 0
    private val item_type_book = 1

    abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        abstract fun bindData(item: ReportItemModel)

    }

    class VideoItemHolder(view: View) : BaseViewHolder(view) {
        var textView: TextView = view.findViewById(R.id.r_txt_video_name)
        var desc: TextView = view.findViewById(R.id.r_txt_video_topic)

        override fun bindData(item: ReportItemModel) {
            textView.text = item.name
            desc.text = item.topicName
        }
    }

    class BookItemHolder(view: View) : BaseViewHolder(view) {
        var textView: TextView = view.findViewById(R.id.r_txt_book_name)
        var desc: TextView = view.findViewById(R.id.r_txt_book_topic)

        override fun bindData(item: ReportItemModel) {
            textView.text = item.name
            desc.text = item.topicName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == item_type_video) {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_video, parent, false)
            VideoItemHolder(view)
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
            BookItemHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        Log.e("error khushboo", "value:: ${list[position].itemType}")
        return if (list[position].itemType == ItemTypeEnum.TYPE_VIDEO.getItemType()) {
            item_type_video
        } else {
            item_type_book
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int = list.size
}