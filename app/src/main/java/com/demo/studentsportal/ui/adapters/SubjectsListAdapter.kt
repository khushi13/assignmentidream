package com.demo.studentsportal.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.demo.studentsportal.R
import com.demo.studentsportal.ui.datamodels.SubjectModel
import com.demo.studentsportal.ui.interfaces.ISubjectClickListener

class SubjectsListAdapter(
    private val subjectList: List<SubjectModel>,
    private val subjectClickListener: ISubjectClickListener
) :
    RecyclerView.Adapter<SubjectsListAdapter.SubjectItemHolder>() {

    class SubjectItemHolder(view: View) : RecyclerView.ViewHolder(view) {
        var textView: TextView = view.findViewById(R.id.r_txt_subject_name)

        fun bindData(subject: SubjectModel) {
            textView.text = subject.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_subject, parent, false)
        return SubjectItemHolder(view)
    }

    override fun onBindViewHolder(holder: SubjectItemHolder, position: Int) {
        holder.bindData(subjectList[position])
        holder.textView.setOnClickListener { subjectClickListener.onSubjectClick(subjectList[position].id) }
    }

    override fun getItemCount(): Int = subjectList.size
}