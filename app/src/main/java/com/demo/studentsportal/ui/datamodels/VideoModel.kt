package com.demo.studentsportal.ui.datamodels

data class VideoModel(
    val videoName: String,
    val id: String,
    val onlineLink: String,
    val topicName: String
)
