package com.demo.studentsportal.ui.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.studentsportal.R
import com.demo.studentsportal.databinding.ActivityReportBinding
import com.demo.studentsportal.provider.SingletonProvider
import com.demo.studentsportal.ui.adapters.ReportAdapter
import com.demo.studentsportal.ui.datamodels.ReportItemModel
import com.demo.studentsportal.ui.utils.UIConstants
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import org.json.JSONArray
import org.json.JSONObject

class ReportActivity : BaseActivity() {

    private lateinit var mBinding: ActivityReportBinding

    override fun getActivityName(): String = ReportActivity::class.java.simpleName

    override fun bindLayout() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_report)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showProgressDialog()
        SingletonProvider.getDatabaseInstance().reference
            .child(SingletonProvider.getFirebaseAuthInstance().currentUser!!.uid)
            .child((UIConstants.FirebaseConstants.student_history))
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    hideProgressDialog()

                    if (!snapshot.exists())
                        return

                    debugLog("onDataChange() with snapshot: ${snapshot.value}")
                    val list = snapshot.value
                    val reportList: ArrayList<ReportItemModel> = arrayListOf()

                    when (list) {
                        is JSONArray -> {
                            val jsonArr: ArrayList<JSONObject> = arrayListOf()

                            for (i in 0 until list.length()) {
                                jsonArr.add(list.getJSONObject(i))
                            }

                            for (json in jsonArr) {
                                val model = ReportItemModel(
                                    name = json.getString("name"),
                                    id = json.getString("id"),
                                    itemType = json.getString("itemType"),
                                    onlineLink = json.getString("onlineLink"),
                                    topicName = json.getString("topicName"),
                                    isOpened = json.getBoolean("opened")
                                )
                                reportList.add(model)
                            }
                        }
                        is ArrayList<*> -> {
                            for (item in list) {
                                debugLog("item:: $item")
                                val k = item as Map<String, String>
                                reportList.add(
                                    ReportItemModel(
                                        name = k.getValue("name"),
                                        itemType = k.getValue("itemType"),
                                        onlineLink = k.getValue("onlineLink"),
                                        topicName = k.getValue("topicName"),
                                        isOpened = k.getValue("opened") as Boolean,
                                        id = k.getValue("id")
                                ))
                                debugLog("list: $reportList")
                            }
                        }
                    }

                    if (reportList.isNullOrEmpty()) {
                        showToast("Something went wrong!!!")
                        return
                    }

                    debugLog("success:: list= $reportList")
                    setAdapter(reportList)
                }

                override fun onCancelled(error: DatabaseError) {
                    hideProgressDialog()
                    debugLog("onCancelled:: error: ${error.message}")
                    showToast("Error: ${error.message}")
                }

            })

            /*.get().addOnSuccessListener {
                debugLog("history success :: ${it.value}")
                hideProgressDialog()
                if (it.value == null) {
                    return@addOnSuccessListener
                }

                val list = it.value
                val reportList: ArrayList<ReportItemModel> = arrayListOf()

                if (list is ArrayList<*>) {
                    reportList.addAll(list as Collection<ReportItemModel>)
                }
                else if (list is JSONArray) {
                    val jsonArr: ArrayList<JSONObject> = arrayListOf()

                    for (i in 0 until list.length()) {
                        jsonArr.add(list.getJSONObject(i))
                    }

                    for (json in jsonArr) {
                        val model = ReportItemModel(
                            name = json.getString("name"),
                            id = json.getString("id"),
                            itemType = json.getString("itemType"),
                            onlineLink = json.getString("onlineLink"),
                            topicName = json.getString("topicName"),
                            isOpened = json.getBoolean("opened")
                        )
                        reportList.add(model)
                    }
                }

                debugLog("khushboo:: list= $list")
                debugLog("khushboo:: list= $reportList")

                val adapter = ReportAdapter(reportList)
                mBinding.rvHistory.layoutManager = LinearLayoutManager(this)
                mBinding.rvHistory.adapter = adapter
            }.addOnFailureListener {
                hideProgressDialog()
                showToast("Something went wrong!!!")
            }*/
    }

    private fun setAdapter(list: ArrayList<ReportItemModel>) {
        val adapter = ReportAdapter(list)
        mBinding.rvHistory.layoutManager = LinearLayoutManager(this)
        mBinding.rvHistory.adapter = adapter
    }

}