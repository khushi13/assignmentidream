package com.demo.studentsportal.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.demo.studentsportal.R
import com.demo.studentsportal.ui.datamodels.VideoModel
import com.demo.studentsportal.ui.interfaces.IVideoClickListener

class VideosListAdapter(
    private val videos: List<VideoModel>,
    private val videoClickListener: IVideoClickListener
) :
    RecyclerView.Adapter<VideosListAdapter.VideoItemHolder>() {

    class VideoItemHolder(view: View) : RecyclerView.ViewHolder(view) {
        var textView: TextView = view.findViewById(R.id.r_txt_video_name)
        var desc: TextView = view.findViewById(R.id.r_txt_video_topic)

        fun bindData(video: VideoModel) {
            textView.text = video.videoName
            desc.text = video.topicName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_video, parent, false)
        return VideoItemHolder(view)
    }

    override fun onBindViewHolder(holder: VideoItemHolder, position: Int) {
        holder.bindData(videos[position])
        holder.itemView.setOnClickListener { videoClickListener.onVideoClick(videos[position]) }
    }

    override fun getItemCount(): Int = videos.size
}