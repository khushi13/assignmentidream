package com.demo.studentsportal.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.databinding.DataBindingUtil
import com.demo.studentsportal.R
import com.demo.studentsportal.databinding.ActivityLoginBinding
import com.demo.studentsportal.ui.db.SharedPrefHelper
import com.demo.studentsportal.provider.SingletonProvider
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    private lateinit var mBinding : ActivityLoginBinding

    override fun getActivityName(): String = LoginActivity::class.java.simpleName

    override fun bindLayout() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btn_login.setOnClickListener {
            if (et_login_email.text.isNullOrEmpty() ||
                    et_login_password.text.isNullOrEmpty()) {
                showToast("Enter all fields...")
                return@setOnClickListener
            } else if (!Patterns.EMAIL_ADDRESS.matcher(et_login_email.text).matches()) {
                showToast("Email address is in wrong format.")
                return@setOnClickListener
            }
            showProgressDialog()
            SingletonProvider.getFirebaseAuthInstance().signInWithEmailAndPassword(
                et_login_email.text.toString(), et_login_password.text.toString()
            ).addOnCompleteListener(this){task ->
                hideProgressDialog()
                if (task.isSuccessful) {
                    debugLog("successfully logged in")
                    SharedPrefHelper.saveUserLoggedIn(this)
                    moveToHomePage()
                } else {
                    debugLog("login failure")
                    showToast("Error during login.")
                }
            }
        }

        txt_login_sign_up.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

    }

    private fun moveToHomePage() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }
}