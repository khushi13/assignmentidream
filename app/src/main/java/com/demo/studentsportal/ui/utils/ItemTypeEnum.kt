package com.demo.studentsportal.ui.utils

enum class ItemTypeEnum(private val value: String) {

    TYPE_VIDEO("TYPE_VIDEO"),
    TYPE_BOOK("TYPE_BOOK");

    fun getItemType(): String = value
}