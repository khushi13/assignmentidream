package com.demo.studentsportal.ui.datamodels

data class ReportItemModel(
    val name: String,
    val id: String,
    val onlineLink: String,
    val topicName: String,
    val itemType: String,
    val isOpened: Boolean
)
