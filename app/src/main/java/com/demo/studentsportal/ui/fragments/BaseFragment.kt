package com.demo.studentsportal.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.demo.studentsportal.ui.activities.BaseActivity
import com.demo.studentsportal.ui.interfaces.IBaseFragmentView

abstract class BaseFragment : Fragment(), IBaseFragmentView {

    abstract fun getFragmentName(): String

    private var fragmentName: String = BaseFragment::class.java.simpleName

    override fun onStart() {
        super.onStart()
        fragmentName = getFragmentName()
    }

    override fun showToast(msg: String) {
        (activity as BaseActivity).showToast(msg)
    }

    override fun debugLog(msg: String) {
        Log.d(fragmentName, msg)
    }

    override fun showProgressDialog() {
        debugLog("showProgressDialog")
        (activity as BaseActivity).showProgressDialog()
    }

    override fun hideProgressDialog() {
        (activity as BaseActivity).hideProgressDialog()
    }

}