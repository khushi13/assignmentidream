package com.demo.studentsportal.ui.db;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefHelper {

    private static final String storage = "studentLogin";
    private static final String isUserLoggedIn = "is_user_logged_in";

    private static SharedPreferences getSharedPref(Context context) {
        return context.getSharedPreferences(storage, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        return getSharedPref(context).edit();
    }

    public static void saveUserLoggedIn(Context context) {
        getEditor(context).putBoolean(isUserLoggedIn, true).commit();
    }

    public static boolean getIfUserLoggedIn(Context context) {
        return getSharedPref(context).getBoolean(isUserLoggedIn, false);
    }

    public static void clearStorage(Context context) {
        getEditor(context).remove(isUserLoggedIn).commit(); }
}
