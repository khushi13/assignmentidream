package com.demo.studentsportal.ui.datamodels

data class BookModel(
    val bookName: String,
    val id: String,
    val onlineLink: String,
    val topicName: String
)
