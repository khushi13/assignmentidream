package com.demo.studentsportal.ui.interfaces

import com.demo.studentsportal.ui.datamodels.VideoModel

interface IVideoClickListener {

    fun onVideoClick(videoModel: VideoModel)
}