package com.demo.studentsportal.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.demo.studentsportal.R
import com.demo.studentsportal.ui.datamodels.BookModel
import com.demo.studentsportal.ui.interfaces.IBookClickListener

class BooksListAdapter(
    private val books: List<BookModel>,
    private val bookClickListener: IBookClickListener
) :
    RecyclerView.Adapter<BooksListAdapter.BookItemHolder>() {

    class BookItemHolder(view: View) : RecyclerView.ViewHolder(view) {
        var textView: TextView = view.findViewById(R.id.r_txt_book_name)
        var desc: TextView = view.findViewById(R.id.r_txt_book_topic)

        fun bindData(book: BookModel) {
            textView.text = book.bookName
            desc.text = book.topicName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
        return BookItemHolder(view)
    }

    override fun onBindViewHolder(holder: BookItemHolder, position: Int) {
        holder.bindData(books[position])
        holder.itemView.setOnClickListener { bookClickListener.onBookClick(books[position]) }
    }

    override fun getItemCount(): Int = books.size
}