package com.demo.studentsportal.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.studentsportal.R
import com.demo.studentsportal.databinding.FragmentBooksBinding
import com.demo.studentsportal.databinding.FragmentVideosBinding
import com.demo.studentsportal.provider.SingletonProvider
import com.demo.studentsportal.ui.DataHelper
import com.demo.studentsportal.ui.adapters.BooksListAdapter
import com.demo.studentsportal.ui.adapters.VideosListAdapter
import com.demo.studentsportal.ui.datamodels.ReportItemModel
import com.demo.studentsportal.ui.datamodels.VideoModel
import com.demo.studentsportal.ui.db.SharedPrefHelper
import com.demo.studentsportal.ui.interfaces.IVideoClickListener
import com.demo.studentsportal.ui.utils.ItemTypeEnum
import com.demo.studentsportal.ui.utils.UIConstants

class VideosFragment : BaseFragment(), IVideoClickListener {

    private lateinit var mBinding: FragmentVideosBinding
    private lateinit var selectedSubjectId: String

    override fun getFragmentName(): String = VideosFragment::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_videos, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.takeIf { it.containsKey(UIConstants.SUBJECT_ID) }?.apply {
            selectedSubjectId = this.getString(UIConstants.SUBJECT_ID)!!
        }

        val list = DataHelper.getSubjectWiseVideosList(
            requireActivity(),
            SingletonProvider.getStudentClass(), selectedSubjectId
        )
        if (list != null && list.isNotEmpty()) {
            mBinding.tvNoVideos.visibility = View.GONE
            val adapter = VideosListAdapter(list, this)
            mBinding.rvVideosList.layoutManager = LinearLayoutManager(requireActivity())
            mBinding.rvVideosList.adapter = adapter

        } else {
            mBinding.tvNoVideos.visibility = View.VISIBLE
        }
    }

    override fun onVideoClick(videoModel: VideoModel) {
        debugLog("onVideoClick():: model=$videoModel")
        val model = ReportItemModel(
            name = videoModel.videoName,
            id = videoModel.id,
            onlineLink = videoModel.onlineLink,
            topicName = videoModel.topicName,
            itemType = ItemTypeEnum.TYPE_VIDEO.getItemType(),
            isOpened = true
        )
        SingletonProvider.HistoryManager.addItemToHistory(model)
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(videoModel.onlineLink)))
    }
}