package com.demo.studentsportal.ui.interfaces

interface ISubjectClickListener {

    fun onSubjectClick(id: String)
}