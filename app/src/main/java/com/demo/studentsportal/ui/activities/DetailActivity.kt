package com.demo.studentsportal.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import com.demo.studentsportal.R
import com.demo.studentsportal.databinding.ActivityDetailBinding
import com.demo.studentsportal.provider.SingletonProvider
import com.demo.studentsportal.ui.adapters.ViewPagerAdapter
import com.demo.studentsportal.ui.utils.UIConstants
import com.google.android.material.tabs.TabLayoutMediator

class DetailActivity : BaseActivity() {

    private lateinit var mBinding: ActivityDetailBinding
    private lateinit var subjectId: String

    override fun getActivityName(): String = DetailActivity::class.java.simpleName

    override fun bindLayout() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.extras == null || !intent.extras!!.containsKey(UIConstants.SUBJECT_ID) ||
            intent.extras!!.getString(UIConstants.SUBJECT_ID) == null
        ) {
            showToast("subject id is null")
            finish()
        } else {
            subjectId = intent.extras!!.getString(UIConstants.SUBJECT_ID)!!
        }

        val pagerAdapter = ViewPagerAdapter(subjectId, this)
        mBinding.viewPager.adapter = pagerAdapter

        TabLayoutMediator(mBinding.tabLayout, mBinding.viewPager) { tab, position ->
            tab.text = if (position == 0) {
                UIConstants.VIDEOS
            } else {
                UIConstants.BOOKS
            }
        }.attach()

        mBinding.ivHistory.setOnClickListener {
            startActivity(Intent(this, ReportActivity::class.java))
        }
    }

/*
    override fun onPause() {
        super.onPause()
        if (SingletonProvider.getHistory().isNotEmpty()) {
            SingletonProvider.getDatabaseInstance().reference
                .child(SingletonProvider.getFirebaseAuthInstance().currentUser!!.uid)
                .child((UIConstants.FirebaseConstants.student_history))
                .setValue(SingletonProvider.getHistory())
        }

    }
*/
}