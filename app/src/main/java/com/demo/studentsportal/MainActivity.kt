package com.demo.studentsportal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.demo.studentsportal.ui.activities.BaseActivity
import com.demo.studentsportal.ui.activities.HomeActivity
import com.demo.studentsportal.ui.activities.LoginActivity
import com.demo.studentsportal.ui.activities.SignUpActivity
import com.demo.studentsportal.ui.db.SharedPrefHelper

class MainActivity : BaseActivity() {
    override fun getActivityName(): String = MainActivity::class.java.simpleName

    override fun bindLayout() {
        // can skip if setContentView is in onCreate
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (SharedPrefHelper.getIfUserLoggedIn(this)) {
            debugLog("user logged in")
            moveToHomePage()
        } else {
            Handler().postDelayed({
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                this.finish()
            }, 2000)
        }
    }

    private fun moveToHomePage() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }
}